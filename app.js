const express = require("express");

const ejs = require("ejs"); // send to Vince
const path = require("path");
const port = 3000;
const fs = require("fs");
const multer = require("multer");
const photosArray = [];


const app = express();
//set storage engine

const storage = multer.diskStorage({
  destination: "./public/uploads/",
  filename: function (req, file, cb) {
    cb(
      null,
      file.fieldname + "-" + Date.now() + path.extname(file.originalname)
    );
  }
});

//init upload

const upload = multer({
  storage: storage,
  limits: { fileSize: 3000000 },
  fileFilter: function (req, file, cb) {
    checkFileType(file, cb);
  },
}).single("uploadPhoto");

//check file type
function checkFileType(file, cb) {
  //allowed ext
  const filetypes = /jpeg|jpg|png|gif/;
  //check ext
  const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
  //check mime
  const mimetype = filetypes.test(file.mimetype);

  if (mimetype && extname) {
    return cb(null, true);
  } else {
      cb("Error: Images Only")
  }
}


//public folder
app.use(express.static("./public/uploads"));


//set route to public folder
app.get("/", (req, res) => {
  const path = "./public/uploads";
    fs.readdir(path,function(err, items){
      const photos = items.map((item) =>{
        return `<img  style= "width:20%; margin: auto;" src= "${item}">`
      })
      res.send(`<h1>Welcome to KenzieGram!</h1><form action ="/upload" method="POST" enctype="multipart/form-data">
      <fieldset>
        <legend>Photo Upload</legend>
        <label>Choose your photo here!</label>
        <input name=uploadPhoto type="file" placeholder="Type something…">
        <span class="help-block">ex. "photo.jpg"</span>
        <label class="checkbox">
          <input type="checkbox"> Mental Health Check &hearts;
        </label>
        <button type="submit" class="btn">Upload</button>
      </fieldset>
    </form> ${photos}`)
    })
    
  });


app.post("/upload/", upload, (req, res, next) => {

  photosArray.push(req.file.filename)
  console.log("Upload:" + req.file.filename)
  // res.redirect(200, `upl/${req.file.filename}`)
  res.send(`<h1>Upload Success!</h1><a href='/'><button>Back home!</button></a><img src="${req.file.filename}" alt="uploaded picture">`)
next()
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
